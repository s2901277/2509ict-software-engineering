# 2509ICT Software Engineering Assignment - Team Group 1

## Group Members
+ Carlisle Miller - s2915824
+ Corey Wells	  - s2884470
+ Nathan Judson   - S2893941
+ Nhan La         - S2816889
+ Rhys Williams   - S2901277
+ Tyron Martin    - S2870648



## How to Install

1. Download & install [node](https://nodejs.org/en/)
2. Download & install [git](https://git-scm.com/downloads)
3. [Clone the repository](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html)
4. Open the repository directory
5. Execute: ```npm run-script installdeps```
6. Execute: ```npm install --all```
7. Execute: ```npm run-script run```

## Tools Used

+ [Webpack](webpack.github.io)

## Major Frameworks and Libraries Used

+ [AngularJS](https://angularjs.org/)
+ [AngularUI Router](https://github.com/angular-ui/ui-router)
+ [Angular Material](https://material.angularjs.org)
+ [LokiJS](http://lokijs.org/)
