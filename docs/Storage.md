# Storage Documentation

## Customer Store
Note: The leading '0' must be stripped from phone numbers & added back when displayed in the view.
```
customerID: 1
customers: {
  "0" : {
    "id": 0,
    "name": "Zaphod Beeblebrox",
    "phone": "0423086359",
    "address1": "26/167 central st",
    "address2": "",
    "suburb": "Labrador"
  },
  "1" : {
    "id": 1,
    "name": "Trillian",
    "phone": "0423086360",
    "address1": "26/167 central st",
    "address2": "",
    "suburb": "Labrador"
  }
}

```

## Item Store
```
itemID: 1
items: {
  "0" : {
    "id": 0,
    "name": "Hamburger",
    "price": 12,
    "type": ["meal"]
  },
  "1" : {
    "id": 1,
    "name": "Coke",
    "price": 3,
    "type": ["side", "drink"]
  }
}

```

## Order Store
```
orderID: 1
orders: {
  "0": {
    "id": 0,
    "time": "2015-12-07T00:58:17.809Z",
    "customer": 0,
    "total": 15,
    "items": [
      {
        "item": 0,
        "price": 12,
        "subTotal": 15,
        "notes": "",
        "sides": [
          {
            "item": 1,
            "price": 3,
            "notes": ""
          }
        ]
      }
    ]
  },
  "1": {
    "id": 1,
    "time": "2015-12-07T01:04:24.894Z",
    "customer": 1,
    "total": 30,
    "items": [
      {
        "item": 0,
        "price": 12,
        "subTotal": 15,
        "notes": "",
        "sides": [
          {
            "item": 1,
            "price": 3,
            "notes": ""
          }
        ]
      },
      {
        "item": 0,
        "price": 12,
        "subTotal": 15,
        "notes": "no tomato",
        "sides": [
          {
            "item": 1,
            "price": 3,
            "notes": "no ice"
          }
        ]
      }
    ]
  }
}
```
