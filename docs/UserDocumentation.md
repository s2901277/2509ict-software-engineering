# User Documentation

Following is simple steps on how to use the system, If you encounter any issues please contact support.

## How to create or edit an user
```
1. Create a new customer 
  a. Updating or adding a User is done via the order customer screen which is located in the "ORDER" tab in the left hand menu.
  b. Fill out the provided fields in the left hand window called "Create New Customer". 
  c. Once all fields have been entered save the customer by pressing/clicking the "Save" button at the bottom of the form.

2. Edit an existing customer
  a. Search for the customer with the "Finds Existing Customer" field using a provided phone number.
  b. Once a customer has been found, you can Edit the customer by changing the details on the left hand window which is now called "Edit Existing Customer".
```
## How to add or edit an order
```
3. Create New Order
  a. After selecting a customer, You can now add items to the order by inputing their corrosponding numbers in the input field shown.
  b. You can also change the price and add notes to each item in the order by changin the fields provided.
  c. Once all items are added you can Press the order button to continue to the receipt screen.
  d. If every item is correct you can press done otherwise you can press edit to correct the current order.
```
## Paying off or deleting orders
```
4. Paying off an order
  a. To pay off an order you open the "UNPAID ORDERS" tab to view all unpaid orders.
  b. Simply press the check box for an order to pay off that order.

5. Deleting unpaid order
  a. To delete an inpaid order simply delete that order by pressing the delete button corrodponding to that item.
```
## Viewing Daily Summaries
```
6. Viewing the current day summaries 
  a. Open the Summaries tab located in management in the left hand menu.
  b. If any orders for the current day this will display as the 2 large blue boxes at the top of the screen labeld todays current totals.

7. Viewing summaries for all days with the total.
  a. Open the Summaries tab located in management in the left hand menu.
  b. COES Sales (If any) will be displayed in the table
```
## Updating menu items
```
8. Adding or removing menu items
  a. Open the "UPDATE MENU" tab located in management in the left hand menu.
  b. Filling out the add menu item form and submitting the firm will add a menu item
  c. Pressing Delete on a existing menu item will delete the corrosponding menu item
  d. Editing the menu item by pressing the edit button and updating the form items will update the menu item.
  ```