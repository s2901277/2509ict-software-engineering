#!/bin/sh

command -v markdown-pdf >/dev/null 2>&1 || { echo >&2 "I require markdown-pdf but it's not installed.  install with: 'npm install -g markdown-pdf'"; exit 1; }

cat docs/docgenHeader.md > docs/sources.md

echo "## Core Application Javascript Source" >> docs/sources.md
find ./src -type f -name '*.js' -print | while read filename; do
    echo "### $filename"
    echo "\`\`\`javascript"
    cat "$filename"
    echo "\`\`\`"
done >> docs/sources.md

echo "## Inital Menu Store Files" >> docs/sources.md
find ./src -type f -name '*.json' -print | while read filename; do
    echo "### $filename"
    echo "\`\`\`json"
    cat "$filename"
    echo "\`\`\`"
done >> docs/sources.md

echo "## Application View Templates" >> docs/sources.md

echo "### index.html" >> docs/sources.md
echo "\`\`\`html" >> docs/sources.md
cat "index.html" >> docs/sources.md
echo "\`\`\`" >> docs/sources.md

find ./views -type f -name '*.html' -print | while read filename; do
    echo "### $filename"
    echo "\`\`\`html"
    cat "$filename"
    echo "\`\`\`"
done >> docs/sources.md

echo "## Application View Style Sheets" >> docs/sources.md
find ./styles -type f -name '*' -print | while read filename; do
    echo "### $filename"
    echo "\`\`\`scss"
    cat "$filename"
    echo "\`\`\`"
done >> docs/sources.md

echo "## Automated Tests" >> docs/sources.md
find ./test -type f -name '*.js' -print | while read filename; do
    echo "### $filename"
    echo "\`\`\`javascript"
    cat "$filename"
    echo "\`\`\`"
done >> docs/sources.md

cat docs/docgenHeader.md >> docs/sources.md

markdown-pdf docs/sources.md

rm docs/sources.md

open docs/sources.pdf
