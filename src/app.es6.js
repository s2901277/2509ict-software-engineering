import angular    from 'angular';
import uirouter   from 'angular-ui-router';
import ngMaterial from 'angular-material';
import ngAnimate  from 'angular-animate';
import ngAria     from 'angular-aria';
import ngMdIcons  from 'angular-material-icons'
import ngMessages from 'angular-messages';
import ngStorage  from 'ngstorage';
require('angular-material/angular-material.scss');
require('angular-material/angular-material.layouts.css');

require('../styles/main.scss');

import routes from './config/uirouter.es6.js';
import store  from './config/store.es6.js';
import mdDate from './config/mdDate.es6.js';
import theme from './config/theme.es6.js';

import StoreService from './services/StoreService.es6.js';

import StoreModelFactory from './factories/StoreModelFactory.es6';
import CustomerFactory  from './models/Customer.es6';
import ItemFactory  from './models/Item.es6';
import OrderFactory  from './models/Order.es6';

import OrderCustomerController      from './controllers/order/OrderCustomerController.es6.js';
import OrderItemsController         from './controllers/order/OrderItemsController.es6.js';
import OrderReceptController        from './controllers/order/OrderReceptController.es6.js';
import OutstandingOrdersController  from './controllers/order/OutstandingOrdersController.es6.js';

import AdminSummaryController	from './controllers/admin/AdminSummaryController.es6.js';
import AdminMenuController		from './controllers/admin/AdminMenuController.es6.js';

import orderByDate from './filters/orderByDate.es6.js'

angular.module('app', ['ui.router', 'ngMaterial', 'ngMdIcons', 'ngStorage', 'ngMessages'])
.config( routes )
.config( store )
.config( mdDate )
.config( theme )
.service('StoreService', StoreService)
.service('StoreModelFactory', StoreModelFactory)
.filter('orderByDate', orderByDate)
.controller('OrderCustomerController', OrderCustomerController)
.controller('OrderItemsController', OrderItemsController)
.controller('OrderReceptController', OrderReceptController)
.controller('OutstandingOrdersController', OutstandingOrdersController)
.controller('AdminSummaryController', AdminSummaryController)
.controller('AdminMenuController', AdminMenuController);
