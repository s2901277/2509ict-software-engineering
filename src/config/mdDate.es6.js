import moment from "moment";

export default function mdDate($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = (date) => {
        return moment(date).format('YYYY-MM-DD');
    };
}

mdDate.$inject = ['$mdDateLocaleProvider'];
