import _ from "lodash";

import Customer from "../models/Customer.es6.js"
import Item     from "../models/Item.es6.js"
import Order    from "../models/Order.es6.js"

export default function store($localStorageProvider) {
    let customers = $localStorageProvider.get(Customer.name);
    let items     = $localStorageProvider.get(Item.name);
    let orders    = $localStorageProvider.get(Order.name);

    if (__DEV__ || !customers){
        let customerStore = require('../store/customers.json');

        $localStorageProvider.set(`${Customer.name}Id`, largestId(customerStore));
        $localStorageProvider.set(Customer.name, customerStore);
    }

    if (__DEV__ || !items){
        let itemStore = require('../store/items.json');

        $localStorageProvider.set(`${Item.name}Id`, largestId(itemStore));
        $localStorageProvider.set(Item.name, itemStore);
    }

    if (__DEV__ || !orders){
        let orderStore = require('../store/orders.json');

        $localStorageProvider.set(`${Order.name}Id`, largestId(orderStore));
        $localStorageProvider.set(Order.name, orderStore);
    }
}

store.$inject = ['$localStorageProvider'];

function largestId(store){
  return _(store).pluck('_id').reduce((acc, cust) => {
    return cust > acc ? cust : acc;
  }, 0);
}
