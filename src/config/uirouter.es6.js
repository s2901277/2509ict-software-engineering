export default function routes($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/order/customer');
  $stateProvider.state('admin', {
      abstract: true,
      url: '/admin',
      template: '<ui-view/>'
    }).state('admin.summary', {
      url: '/summaries',
      template: require('../../views/admin/summary.html'),
      controller: 'AdminSummaryController',
      controllerAs: 'admin'
    }).state('admin.menu', {
      url: '/update-menu',
      template: require('../../views/admin/menuupdates.html'),
      controller: 'AdminMenuController',
      controllerAs: 'admin'
    }).state('order', {
      abstract: true,
      url: '/order',
      template: '<ui-view/>'
    }).state('order.customer', {
      url: '/customer',
      template: require('../../views/order/OrderCustomer.html'),
      controller: 'OrderCustomerController',
      controllerAs: 'ctrl'
    }).state('order.items', {
      url: '/items/:customer/:order',
      template: require('../../views/order/OrderItems.html'),
      controller: 'OrderItemsController',
      controllerAs: 'ctrl'
    }).state('order.recept', {
      url: '/recept/:order',
      template: require('../../views/order/OrderRecept.html'),
      controller: 'OrderReceptController',
      controllerAs: 'ctrl'
    }).state('order.outstading', {
      url: '/outstanding',
      template: require('../../views/order/OutstandingOrders.html'),
      controller: 'OutstandingOrdersController',
      controllerAs: 'ctrl'
    }).state('docs', {
      abstract: true,
      url: '/docs',
      template: '<ui-view/>'
    }).state('docs.readme', {
      url: '/readme',
      template: require('../../README.md')
    }).state('docs.userdocumentation', {
      url: '/userdocumentation',
      template: require('../../docs/UserDocumentation.md')
    });
}

routes.$inject = ['$stateProvider', '$urlRouterProvider'];
