import ItemCollection from "../../models/ItemCollection.es6.js";
import Item           from "../../models/Item.es6.js";

export default class AdminMenuController {
    constructor($state, $mdToast, $mdDialog, StoreModelFactory) {
        this.StoreModelFactory  = StoreModelFactory;
        this.$mdDialog          = $mdDialog;
        this.$mdToast           = $mdToast;
        this.$state             = $state;

        this.newItem();
        this.refreshItems();
    }

    save(){
        this.item.save();
        this.showToast(this.item.displayName, 'saved');
        this.newItem();
        this.refreshItems();
    }

    editItem(item){
        this.title  = "Edit";
        this.edit   = true;
        this.item   = item;
    }

    newItem(){
        this.title  = 'New'
        this.edit   = false;
        this.item   = this.StoreModelFactory.newModel(Item);
    }

    delete(ev, item){
        let del = this.$mdDialog.confirm()
          .title('Would you like to delete this menu item?')
          .textContent('This will permanently remove this item from the menu.')
          .ariaLabel('Delete Item')
          .targetEvent(ev)
          .ok('Delete')
          .cancel('Cancel');

        this.$mdDialog.show(del).then(() => {
            item.delete();
            this.showToast(item.displayName, 'removed');
            this.refreshItems();
        }, () => {
        });
    }

    refreshItems(){
        this.items = this.StoreModelFactory.getModelCollection(ItemCollection, Item);
    }

    showToast(name, action){
        this.$mdToast.show({
            position: "top right",
            template: `<md-toast>${name} ${action}</md-toast>`
        });
    }
}

AdminMenuController.$inject = ['$state', '$mdToast', '$mdDialog', 'StoreModelFactory'];
