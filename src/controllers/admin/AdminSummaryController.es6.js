import moment from "moment";
import Order           from "../../models/Order.es6.js";
import OrderCollection from "../../models/OrderCollection.es6.js";

export default class AdminSummaryController {
    constructor($state, StoreModelFactory) {
        this.$state         = $state;
        this.StoreService 	= StoreModelFactory;
        this.orders    		  = StoreModelFactory.getModelCollection(OrderCollection, Order);
        this.today 			    = moment(new Date()).format('YYYY-MM-DD');
        this.totalTakings 	= StoreModelFactory.getModelCollection(OrderCollection, Order).takings;
    }
}

AdminSummaryController.$inject = ['$state', 'StoreModelFactory'];
