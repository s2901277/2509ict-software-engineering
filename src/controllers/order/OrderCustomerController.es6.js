import AutocompleteInput  from "../../models/AutocompleteInput.es6.js";
import Customer           from "../../models/Customer.es6.js";
import CustomerCollection from "../../models/CustomerCollection.es6.js";

export default class OrderCustomerController {
    constructor($state, $mdToast, StoreModelFactory) {
        this.$state             = $state;
        this.$mdToast           = $mdToast;
        this.StoreModelFactory  = StoreModelFactory;
        this.customers          = StoreModelFactory.getModelCollection(CustomerCollection, Customer);
        this.customerPicked     = false;

        this.input = this.initInput();

        this.newCustomer();
    }

    initInput(){
        let querySearch = (input) => {
            if(input)
                return this.customers.getByPhone(input);
            else
                return this.customers.collection;
        };
        let onSearchTextChange = undefined;
        let onSelectedItemChange = (customer) => {
            if(customer){
                this.customerPicked = true;
                this.editCustomer(customer);
            }
        };

        return new AutocompleteInput(querySearch, onSearchTextChange, onSelectedItemChange);
    }

    editCustomer(customer){
        this.edit               = true;
        this.customerDetails    = customer;
        this.customerFormHeader = "Edit Existing Customer";
    }

    newCustomer(){
        this.input.clear();
        this.customerPicked     = false;
        this.edit               = false;
        this.customerFormHeader = "Create New Customer";
        this.customerDetails    = this.StoreModelFactory.newModel(Customer);
    }

    save(){
        this.customerDetails.save();
        this.$mdToast.show({
          position: "top right",
          template: `<md-toast>Customer ${this.customerDetails.displayName} added</md-toast>`
        });
        this.customers = this.StoreModelFactory.getModelCollection(CustomerCollection, Customer);
        this.newCustomer();
    }

    order(){
        this.$state.go('order.items', { customer: this.customerDetails._id } );
    }
}

OrderCustomerController.$inject = ['$state', '$mdToast', 'StoreModelFactory'];
