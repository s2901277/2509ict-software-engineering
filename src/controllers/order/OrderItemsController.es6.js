import AutocompleteInput  from "../../models/AutocompleteInput.es6.js";
import ItemCollection     from "../../models/ItemCollection.es6.js"
import Item               from "../../models/Item.es6.js";
import OrderCollection  from "../../models/OrderCollection.es6.js";
import Order              from "../../models/Order.es6.js";
import CustomerCollection from "../../models/CustomerCollection.es6.js";
import Customer           from "../../models/Customer.es6.js";


export default class OrderItemsController {
    constructor($state, $stateParams, StoreModelFactory) {
        this.$state             = $state;
        this.customerId         = $stateParams.customer;
        this.orderId            = $stateParams.order;
        this.storeModelFactory  = StoreModelFactory;

        if(this.orderId !== ""){
            this.orderBtnText = "Save";
            this.order = StoreModelFactory.getModelCollection(OrderCollection, Order).getById(this.orderId)[0];
        } else if(this.customerId !== ""){
            this.orderBtnText = "Order";
            this.order = StoreModelFactory.newModel(Order).setCustomer(this.customerId);
        } else {
            $state.go('order.customer');
        }

        this.items = StoreModelFactory.getModelCollection(ItemCollection, Item);
        this.quantity   = 1;
        this.itemPicker = this.initInput();

        this.customer = StoreModelFactory.getModelCollection(CustomerCollection, Customer).getById(this.customerId)[0];
    }

    initInput(){
      this.selectedItem;
      let querySearch = (input) => {
          if(input){
              return this.items.getById(input);
          } else {
              return this.items.collection;
          }
      };
      let onSearchTextChange = undefined;
      let onSelectedItemChange = (item) => {
          this.selectedItem = new Item().copy(item);
      };

       return new AutocompleteInput(querySearch, onSearchTextChange, onSelectedItemChange, this);
    }

    addItem(){
        if(this.quantity && this.quantity > 0 && this.selectedItem){
            var quantity = this.quantity;
            while(quantity > 0){
                this.order.addItem(this.selectedItem);
                quantity--;
            }
        }
    }

    saveOrder(){
        if(this.order.itemCount > 0){
            this.$state.go('order.recept', { order: this.order.save() });
        }
    }
}

OrderItemsController.$inject = ['$state', '$stateParams', 'StoreModelFactory'];
