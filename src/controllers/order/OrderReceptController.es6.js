import Order            from "../../models/Order.es6.js";
import OrderCollection  from "../../models/OrderCollection.es6.js";
import CustomerCollection from "../../models/CustomerCollection.es6.js";
import Customer           from "../../models/Customer.es6.js";

export default class OrderReceptController {
    constructor($state, $stateParams, StoreModelFactory) {
        this.$state = $state;
        this.orderId = $stateParams.order;
        if(!this.orderId)
            $state.go('order.customer');

        this.order = StoreModelFactory.getModelCollection(OrderCollection, Order).getById(this.orderId)[0];
    }

    done(){
        this.$state.go('order.outstading');
    }

    edit(){
        this.$state.go('order.items', {
            order: this.orderId,
            customer: this.order.customer
        });
    }
}

OrderReceptController.$inject = ['$state', '$stateParams', 'StoreModelFactory'];
