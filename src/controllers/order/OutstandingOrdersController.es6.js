import OrderCollection  from "../../models/OrderCollection.es6.js";
import Order            from "../../models/Order.es6.js";
import CustomerCollection from "../../models/CustomerCollection.es6.js";
import Customer           from "../../models/Customer.es6.js";

export default class OutstandingOrdersController {
    constructor($state, StoreModelFactory) {
        this.$state             = $state;
        this.StoreModelFactory  = StoreModelFactory;
        this.orders     = StoreModelFactory.getModelCollection(OrderCollection, Order);
        this.customers  = StoreModelFactory.getModelCollection(CustomerCollection, Customer);
    }
}

OutstandingOrdersController.$inject = ['$state', 'StoreModelFactory'];
