export default class StoreModelFactory {
    constructor(StoreService, storeNames) {
        this.StoreService = StoreService;
    }

    newModel(model){
        return new model(this);
    }

    getModelCollection(collection, model){
        return new collection(this.StoreService.getStore(model), this);
    }

    save(model){
        if(model._id === undefined){
            let id = this.StoreService.getId(model.constructor);
            model._id = id;
        }
        this.StoreService.save(model);
        return model._id;
    }

    delete(model){
        if(model._id === undefined){
            throw("must delete model with _id");
        }
        this.StoreService.delete(model);
    }
}

StoreModelFactory.$inject = ['StoreService'];
