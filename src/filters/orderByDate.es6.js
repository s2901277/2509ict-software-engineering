import _      from 'lodash';

export default function orderByDate(){
    return function(items) {
        return _(items).values().sortByOrder((takings) => {
            return new Date(takings.day).getTime() / 1000;
        }, ['desc']).value();
    };
}
