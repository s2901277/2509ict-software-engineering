export default class AutocompleteInput {
    constructor(querySearch, onSearchTextChange, onSelectedItemChange, controller, cache = false, disabled = false) {
        this.searchText;
        this.selectedItem;
        this.controller   = controller;
        this.cache        = cache;
        this.disabled     = disabled;
        this.querySearch  = querySearch;
        this.onSearchTextChange   = onSearchTextChange;
        this.onSelectedItemChange = onSelectedItemChange;
    }

    clear(){
        this.searchText = "";
    }
}
