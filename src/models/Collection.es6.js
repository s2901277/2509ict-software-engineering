import StoreModel from './StoreModel.es6.js';

export default class Collection extends StoreModel {
    constructor(collection, factory) {
        super(factory);
        if( collection === undefined || collection === null ){
            collection = {};
        }
        this.collection = collection;
    }

    add(item){
        this.collection[item._id] = item;
        return this;
    }

    getById(id){
        return [this.collection[id]];
    }

}
