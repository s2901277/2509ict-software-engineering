import _ from "lodash";

import StoreModel from './StoreModel.es6.js';

export default class Customer extends StoreModel {
    constructor(customerFactory) {
        super(customerFactory);
    }

    copy(customer){
        this._id          = customer._id;
        this.displayName  = customer.displayName;
        this.phone        = customer.phone;
        this.address1     = customer.address1;
        this.address2     = customer.address2;
        this.suburb       = customer.suburb;

        return this;
    }

    save(){
        return this.factory.save(this);
    }

    delete(){
        if(this._id !== undefined)
            this.factory.delete(this);
    }
}
