import Collection from './Collection.es6.js';
import Customer   from './Customer.es6.js';

export default class CustomerCollection extends Collection {
    constructor(collection, customerFactory) {
        super(collection, customerFactory);
        this.collection = _(collection).map((cust) => {
            return new Customer(customerFactory).copy(cust);
        }).value();
    }

    getByName(name){
      name = name.toLowerCase();
      return _(this.collection).filter((cust) => {
          return cust.displayName.toLowerCase().includes(name);
      }).value();
    }

    getByPhone(phone){
      return _(this.collection).filter((cust) => {
          return cust.phone.includes(phone);
      }).value();
    }
}
