import StoreModel from './StoreModel.es6.js';

export default class Item extends StoreModel {
    constructor(itemFactory) {
        super(itemFactory);
    }

    copy(item){
        this._id          = item._id
        this.displayName  = item.displayName;
        this.price        = item.price;
        this.itemTypes    = item.itemType;

        return this;
    }

    save(){
        return this.factory.save(this);
    }

    delete(){
        if(this._id !== undefined)
            this.factory.delete(this);
    }
}
