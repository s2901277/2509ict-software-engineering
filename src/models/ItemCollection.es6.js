import _ from "lodash";

import Collection from './Collection.es6.js';
import Item       from './Item.es6.js';

export default class ItemCollection extends Collection {
    constructor(collection, itemFactory) {
        super(collection, itemFactory);
        this.collection = _(collection).map((item) => {
            return new Item(itemFactory).copy(item);
        }).value();
    }

    getByName(name){
      name = name.toLowerCase();
      return _(this.collection).filter((item) => {
          return item.displayName.toLowerCase().includes(name);
      }).value();
    }
}
