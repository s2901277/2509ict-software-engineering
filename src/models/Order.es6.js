import moment from "moment";
import _      from "lodash";

import StoreModel from './StoreModel.es6.js';

export default class Order extends StoreModel {
    constructor(orderFactory) {
        super(orderFactory);

        this.paid       = false;
        this.itemCount  = 0;
        this.time       = new Date();
        this.customer   = null;
        this.total      = 0;
        this.items      = [];
    }

    get isPaid(){
        return this.paid;
    }

    set isPaid(bool){
        this.paid = bool;
        this.save();
    }

    setCustomer(customer){
        this.customer = customer;

        return this;
    }

    copy(order){
      this._id      = order._id;
      this.paid     = order.paid;
      this.time     = new Date(order.time);
      this.customer = order.customer;
      this.total    = order.total;
      this.items    = order.items;

      return this;
    }

    get itemCount(){
        return this.items.length;
    }

    set itemCount(count){
        // no op
    }

    // returns index
    addItem(item, price = item.price, notes = ""){
        this.items = this.items.concat(new OrderItem(this, item, price, notes));
        this.total += price;
        return this.items.length;
    }

    removeItem(index){
        this.total -= this.items[index].cost;
        this.items.splice(index, 1);
    }

    save(){
        if(!this.time)
            this.time = new Date();

        this.time = this.time.toISOString();
        return this.factory.save(this);
    }

    delete(){
        if(this._id !== undefined)
            this.factory.delete(this);
    }
}

class OrderItem {
    constructor(order, item, price = 0, notes = ""){
        Object.defineProperty(this, 'order', { value: order });
        this.item  = item;
        this.price = price;
        this.notes = notes;
    }

    get cost(){
        return this.price;
    }

    set cost(price){
        this.order.total -= this.price;
        this.order.total += price;
        this.price       =  price;
    }
}
