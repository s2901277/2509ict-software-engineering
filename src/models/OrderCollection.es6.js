import moment from "moment";
import _      from "lodash";

import Collection from './Collection.es6.js';
import Order      from './Order.es6.js';

export default class OrderCollection extends Collection {
    constructor(collection, orderFactory) {
        super(collection, orderFactory);
        this.collection = _(collection).map((order) => {
            return new Order(orderFactory).copy(order);
        }).value();
    }

    getByCustomerId(id){
        return _(this.collection).filter((order) => {
            return order.customer == id;
        }).value();
    }

    getByDate(date){
        return _(this.collection).filter((order) => {
            return moment(date).isSame(order.time, 'day');
        }).value();
    }

    get takings(){
        return _(this.collection).reduce((takings, order) => {
            return takings.addOrder(order);
        }, new Takings());
    }

    get unpaidOrders(){
        return _(this.collection).reduce((collection, order) => {
            if(!order.paid)
                return collection.concat(order);
            else
                return collection;
        }, []);
    }
}

class Takings {
    constructor(){
        this.days   = {};
        this.total  = 0;
        this.count  = 0;
    }

    addOrder(order){
        if(order.paid){
            this.count++;
            this.total += order.total;
            let date = moment(order.time).format('YYYY-MM-DD');
            if(this.days[date] === undefined)
                this.days[date] = new DailyTakings(date);

            this.days[date].addOrder(order);
        }
        return this;
    }
}

class DailyTakings {
    constructor(day){
        this.day    = day;
        this.count  = 0;
        this.orders = {};
        this.total  = 0;
    }

    addOrder(order){
        this.count++;
        this.total += order.total;
        this.orders[order._id] = order;
        return this;
    }
}
