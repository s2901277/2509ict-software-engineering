export default class StoreService {
    constructor($localStorage, $sessionStorage) {
        this.$localStorage    = $localStorage;
        this.$sessionStorage  = $sessionStorage;
    }

    getId(model){
        return ++this.$localStorage[`${model.name}Id`];
    }

    getStore(model){
        return this.$localStorage[model.name];
    }

    save(modelInstance){
        this.$localStorage[modelInstance.constructor.name][modelInstance._id] = modelInstance;
    }

    delete(modelInstance){
        delete this.$localStorage[modelInstance.constructor.name][modelInstance._id];
    }
}

StoreService.$inject = ['$localStorage', '$sessionStorage'];
