import chai from 'chai';

import Collection from '../../src/models/Collection.es6.js'

export default function CollectionTest() {
  let expect = chai.expect;

  describe('Collection', () => {
    let validCollection0 = {
        "0": {
            _id: 0,
            displayName: "test0"
        },
        "1": {
            _id: 1,
            displayName: "test1"
        },
        "2": {
            _id: 2,
            displayName: "test2"
        }
    };

    let validCollection1 = {
        "0": {
            _id: 0,
            displayName: "test0"
        },
        "1": {
            _id: 1,
            displayName: "test1"
        },
        "2": {
            _id: 2,
            displayName: "test2"
        },
        "3": {
            _id: 3,
            displayName: "test3"
        }
    };

    let invalidCollection0 = {
        "0": {
            _id: 0,
            displayName: "test0"
        },
        "1": {
            _id: 1,
            displayName: "test1"
        },
        "2": {
            displayName: "test2"
        },
    };

    describe('#constructor()', () => {
      it('allows empty array', () => {
        expect(() => {
          new Collection({});
        }).to.not.throw(ReferenceError);
      });

      it('accepts valid array', () => {
        expect(() => {
          new Collection(validCollection0);
        }).to.not.throw(ReferenceError);
      });
    });

    describe('#getById(id)', () => {
        let collection;

        beforeEach(() => {
            collection = new Collection(validCollection0);
        });

        it('returns matching element', () => {
            expect( collection.getById(1) ).to.deep.equal([validCollection0[1]]);
        });
        it('returns null when no matching element within collection', () => {
            expect( [undefined] ).to.deep.equal([undefined]);
        });
    });

    describe('#add(item)', () => {
        let collection;

        beforeEach(() => {
            collection = new Collection(validCollection0);
        });

        it('returns new collectin containing new element', () => {
            let newElem = validCollection1[3];

            expect( collection.add(newElem) ).to.deep.equal(new Collection(validCollection1));
        });
    });
  });
}
