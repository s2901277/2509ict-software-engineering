import chai       from 'chai';
import path       from 'path';
import sinon      from 'sinon';
import sinonChai  from 'sinon-chai';

import ModelsTests from './models/ModelsTests.es6.js'

chai.use(sinonChai);

beforeEach(function() {
  // Create a new sandbox before each test
  this.sinon = sinon.sandbox.create();
});

afterEach(function() {
  // Cleanup the sandbox to remove all the stubs
  this.sinon.restore();
});

ModelsTests();
