var path = require('path');
var webpack = require("webpack");

var definePlugin = new webpack.DefinePlugin({
  __DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || false)),
});

module.exports = {
    entry: './src/app.es6.js',
    output: {
        path: __dirname + "/js",
        filename: 'bundle.js'
    },
    plugins: [definePlugin],
    module: {
        loaders: [
            {
              test: path.join(__dirname, 'src'),
              loader: 'babel',
              exclude: /(node_modules|bower_component)/,
            },
            {
              test: /\.html$/,
              loader: "html-loader"
            },
            {
              test: /\.css$/,
              loader: "style!css"
            },
            {
              test: /\.scss$/,
              loaders: ["style", "css", "sass"]
            },
            {
              test: /\.md$/,
              loader: "html!markdown"
            },
            {
              test: /\.json$/,
              loader: "json"
            }
        ]
    },
    node: {
      fs: "empty"
    }
};
